# Intro
I had doubts about which test I would have to do. So I ended up doing backend and frontend.

### Heroku Server
The services can be access for the following link's:

#### Backend
https://yan-shawandpartners-api.herokuapp.com/

#### Frontend
https://yan-shawandpartners-webapp.herokuapp.com/

### Important
The github api limit 60 calls per hour without authenticated. I don't apply my credentials in public repositories for security.

# Resume

In this project was make two packages to attend the requisites.

- shawandpartners-api: NodeJs api that work as a proxy to github public api.
- shawandpartners-webapp: ReactJs web application that call shawandpartners-api and show your users.

## Installation

Use the docker-compose to build and install all services. Run in the project main package:

```bash
docker-compose up -d --build
```
If you are using linux, you can just run bash script build-start.sh:

```bash
 ./build-start.sh 
```

## Manual Installation

If you don't have docker and docker-compose in your computer, you can build and install the services with following commands:

#### Api
In **shawandpartners-api** run:

Build
```bash
npm install
```

Start
```bash
npm run server
```

#### Webapp
In **shawandpartners-webapp** run:

Build
```bash
npm install
```

Start
```bash
npm run start-dev
```

## Tests

In this moment only api service have a tests. You can run with the command in the shawandpartners-api package:

```bash
npm run test
```
This tests are in BDD format with Given, When and Then steps.