'use strict';

const userService = require('../services/user.service');
const logger = require('../infra/logger');

exports.getUsers = async (req, _h) => {
    try {
        const response = await userService.getUsers(req.query);
        return _h.response(response.body || { message: response.message }).code(response.statusCode || 200);
    } catch (e) {
        logger.error(e.message);
        return _h.response({ message: e.message }).code(e.statusCode || 500);
    }
}

exports.getUserDetails = async (req, _h) => {
    try {
        const response = await userService.getUserDetails(req.params.username);
        return _h.response(response.body || { message: response.message }).code(response.statusCode || 200);
    } catch (e) {
        logger.error(e.message);
        return _h.response({ message: e.message }).code(e.statusCode || 500);
    }
}

exports.getUserRepos = async (req, _h) => {
    try {
        const response = await userService.getUserRepos(req.params.username);
        return _h.response(response.body || { message: response.message }).code(response.statusCode || 200);
    } catch (e) {
        logger.error(e.message);
        return _h.response({ message: e.message }).code(e.statusCode || 500);
    }
}