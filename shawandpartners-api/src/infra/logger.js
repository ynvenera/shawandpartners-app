'use strict';

const log4js = require('log4js');

log4js.configure({
    appenders: { out: { type: 'stdout', layout: { type: 'basic' } } },
    categories: { default: { appenders: ['out'], level: 'info' } }
});
const logger = log4js.getLogger();
logger.level = process.env.LOG_LEVEL || 'error';

module.exports = logger;
