'use strict';

const githubuserDto = require('../dtos/githubuser.dto');

exports.builderUsers = async(users) => {
    return await githubuserDto.usersList(users);
}

exports.builderUserDetail = async(userDetail) => {
    return await githubuserDto.userDetails(userDetail);
}

exports.builderUserRepos = async(userRepos) => {
    return await githubuserDto.userRepoList(userRepos);
}