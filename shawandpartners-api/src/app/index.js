'use strict';

const server = require('./server');
const logger = require('../infra/logger');

(async () => {
    try {
        await server.register(require('@hapi/inert'));
        await server.start();
        logger.info(`Server running at: ${server.info.uri}`);
    }
    catch (err) {
        logger.error(err);
    }
})();
