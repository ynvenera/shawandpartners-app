'use strict';

const Hapi = require('@hapi/hapi');
const routesConfig = require('../routes/routes.config');

const server = new Hapi.server({
    port: process.env.PORT || 3001,
    host: process.env.HOST || '0.0.0.0',
    routes: {
        cors: true
    }
});

routesConfig.configure(server);

module.exports = server;