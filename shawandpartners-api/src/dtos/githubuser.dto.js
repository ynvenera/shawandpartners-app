'use strict';

exports.usersList = async (users) => {
  const userList = [];
  for (const user of users) {
    const newUser = {};
    newUser.github_id = user.id;
    newUser.username = user.login;
    newUser.node_id = user.node_id;
    newUser.avatar_url = user.avatar_url;
    newUser.login = user.login;

    userList.push(newUser);
  }

  return userList;
}

exports.userDetails = async (githubUser) => {
  const newUser = {};
  newUser.github_id = githubUser.id;
  newUser.username = githubUser.login;
  newUser.node_id = githubUser.node_id;
  newUser.avatar_url = githubUser.avatar_url;
  newUser.login = githubUser.login;
  newUser.name = githubUser.name;
  newUser.company = githubUser.company;
  newUser.blog = githubUser.blog;
  newUser.location = githubUser.location;
  newUser.email = githubUser.email;
  newUser.bio = githubUser.bio;
  newUser.public_repos = githubUser.public_repos;
  newUser.followers = githubUser.followers;
  newUser.following = githubUser.following;
  return newUser;
}

exports.userRepoList = async (userRepos) => {
  const userRepoList = [];
  for (const repo of userRepos) {
    const newRepo = {};
    newRepo.github_id = repo.id;
    newRepo.node_id = repo.node_id;
    newRepo.name = repo.name;
    newRepo.full_name = repo.full_name;
    newRepo.description = repo.description;
    newRepo.fork = repo.fork;
    userRepoList.push(newRepo);
  }

  return userRepoList;
}