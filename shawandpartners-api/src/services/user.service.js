'use strict';

const logger = require('../infra/logger');
const userBuilder = require('../builders/user.builder');
const githubClient = require('../clients/github.client');

exports.getUsers = async (req) => {
    const { since } = req;
    const users = await githubClient.getUsers(since);
    if (users){
        const usersBuilded = await userBuilder.builderUsers(users);
        return { statusCode: 200, body: usersBuilded };
    }
    return { statusCode: 404, message: 'Users not found.' };
}

exports.getUserDetails = async (username) => {
    const user = await githubClient.getUserDetail(username);
    if (user){
        const userBuilded = await userBuilder.builderUserDetail(user);
        return { statusCode: 200, body: userBuilded };
    }
    return { statusCode: 404, message: 'User not found.' };
}

exports.getUserRepos = async (username) => {
    const repos = await githubClient.getUserRepos(username);
    if (repos){
        const reposBuilded = await userBuilder.builderUserRepos(repos);
        return { statusCode: 200, body: reposBuilded };
    }
    return { statusCode: 404, message: 'Repos not found.' };
}

