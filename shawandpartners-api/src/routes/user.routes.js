'use strict';

const userController = require('../controllers/user.controller');

module.exports = [
    {
        method: 'GET',
        path: '/api/users',
        handler: userController.getUsers
    },
    {
        method: 'GET',
        path: '/api/users/{username}/details',
        handler: userController.getUserDetails
    },
    {
        method: 'GET',
        path: '/api/users/{username}/repos',
        handler: userController.getUserRepos
    }
];
