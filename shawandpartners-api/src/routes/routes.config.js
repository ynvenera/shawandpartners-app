'use strict';

const mainRoutes = require('./main.routes');
const userRoutes = require('./user.routes');

exports.configure = (server) => {
    server.route(mainRoutes.concat(userRoutes));
}