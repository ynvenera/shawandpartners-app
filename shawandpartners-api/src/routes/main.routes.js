'use strict';

const mainController = require('../controllers/main.controller');

module.exports = [
    {
        method: 'GET',
        path: '/openapi.json',
        handler: (request, h) => {
            return h.file('./src/openapi.json').code(200);
        }
    },
    {
        method: '*',
        path: '/{any*}',
        handler: function (request, h) {
            return h.response('404 Error! Page Not Found!').code(404);
        }
    },
    {
        method: 'GET',
        path: '/health',
        handler: mainController.health
    }
];
