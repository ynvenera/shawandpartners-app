'use strict'

const sinon = require('sinon');
const githubClient = require('../clients/github.client')

before(async () => {
  global.sandbox = sinon.createSandbox();
  global.getGithubUsers = global.sandbox.stub(githubClient, "getUsers");
  global.getGithubUserDetails = global.sandbox.stub(githubClient, "getUserDetail");
  global.getGithubUserRepos = global.sandbox.stub(githubClient, "getUserRepos");
});

after(async () => {
  global.sandbox.restore();
});
