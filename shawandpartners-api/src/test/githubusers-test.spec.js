const chai = require('chai')
  , server = require('../app/server')
  , assert = chai.assert;

const githubMock = require('./githubusers.mock');

describe('Given the github service', async () => {
  describe('When I call the users endpoint without parameter', async () => {
    it('Then the status code should be 200 OK', async () => {
      let request = {
        method: 'GET',
        url: '/api/users'
      };

      global.getGithubUsers.returns(Promise.resolve(githubMock.usersList));
      let response = await server.inject(request);
      assert.equal(response.statusCode, 200);
    });
  });

  describe('When I call the users endpoint with since parameter', async () => {
    it('Then the status code should be 200 OK', async () => {
      let request = {
        method: 'GET',
        url: '/api/users?since=20'
      };

      global.getGithubUsers.returns(Promise.resolve(githubMock.usersList));
      let response = await server.inject(request);
      assert.equal(response.statusCode, 200);
    });
  });

  describe('When I call the user details endpoint', async () => {
    it('Then the status code should be 200 OK', async () => {
      let request = {
        method: 'GET',
        url: '/api/users/mojombo/details'
      };

      global.getGithubUserDetails.returns(Promise.resolve(githubMock.userDetails));
      let response = await server.inject(request);
      assert.equal(response.statusCode, 200);
    });
  });

  describe('When I call the user repos endpoint', async () => {
    it('Then the status code should be 200 OK', async () => {
      let request = {
        method: 'GET',
        url: '/api/users/mojombo/repos'
      };

      global.getGithubUserRepos.returns(Promise.resolve(githubMock.userRepos));
      let response = await server.inject(request);
      assert.equal(response.statusCode, 200);
    });
  });
});
