const chai = require('chai')
  , server = require('../app/server')
  , assert = chai.assert;

describe('Given the service is available', async () => {
  describe('When I call the health check endpoint', async () => {
    it('Then the status code should be 200 OK', async () => {
      let request = {
        method: 'GET',
        url: '/health'
      };
      let response = await server.inject(request);
      assert.equal(response.statusCode, 200);
    });
  });
});
