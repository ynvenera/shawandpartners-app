'use strict';

const logger = require('../infra/logger');
const axios = require('axios');

const GITHUB_BASEURL = process.env.GITHUB_BASEURL || 'https://api.github.com/';

exports.getUsers = async(since) => {
    let ep = 'users';
    if (since){
        ep = `${ep}?since=${since}`
    }
    const request = await buildRequest(ep);

    logger.info(`Client call ep: ${request}`)
    const response = await axios.get(request);
    if (response.status !== 200){
        logger.error(`Code: ${response.status} - ${response.statusText}`);
        return null;
    }
    return response.data;
}

exports.getUserDetail = async(username) => {
    const request = await buildRequest(`users/${username}`);

    logger.info(`Client call ep: ${request}`)
    const response = await axios.get(request);
    if (response.status !== 200){
        logger.error(`Code: ${response.status} - ${response.statusText}`);
        return null;
    }
    return response.data;
}

exports.getUserRepos = async(username) => {
    const request = await buildRequest(`users/${username}/repos`);

    logger.info(`Client call ep: ${request}`)
    const response = await axios.get(request);
    if (response.status !== 200){
        logger.error(`Code: ${response.status} - ${response.statusText}`);
        return null;
    }
    return response.data;
}

const buildRequest = async (endpoint) => {
    return `${GITHUB_BASEURL}${endpoint}`
}