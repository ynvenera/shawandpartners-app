import React, { useState, useEffect } from 'react';
import './style.css';

import {FiInfo, FiList} from "react-icons/fi";
import { useNavigate } from 'react-router-dom';

import api from "../../services/api";

export default function User(){

    const [users, setUsers] = useState([]);
    const navigate = useNavigate();

    async function userDetail(username) {
        navigate(`details/${username}`);
    }

    async function userRepos(username) {
        navigate(`repos/${username}`);
    }

    useEffect(() => {
        api.get('api/users').then(response => {
            setUsers(response.data)
        })
    })

    return (
        <div className="user-container">
            <h1> Github Users</h1>
            <ul>
                {users.map(user => (
                    <li>
                        <strong>Github Id:</strong>
                        <p>{user.github_id}</p>
                        <strong>Node Id:</strong>
                        <p>{user.node_id}</p>
                        <strong>Username:</strong>
                        <p>{user.username}</p>

                        <button onClick={() => userDetail(user.username)} type="button">
                            <FiInfo size={20} color="#251FC5"/>
                        </button>

                        <button onClick={() => userRepos(user.username)} type="button">
                            <FiList size={20} color="#251FC5"/>
                        </button>
                    </li>
                    )
                )}
            </ul>
        </div>
    )
}