import React, {useEffect, useState} from 'react';
import {useParams, Link} from 'react-router-dom';
import { FiArrowLeft } from "react-icons/fi";

import './style.css';
import api from "../../services/api";

export default function UserDetail(){
    const { username } = useParams();

    const [userDetail, setUserDetail] = useState([]);

    useEffect(() => {
        api.get(`api/users/${username}/details`).then(response => {
            setUserDetail(response.data)
        })
    })

    return (
        <div className="detail-container">
            <header>
                <span>Github Users - <strong>{username}</strong></span>
            </header>

            <div className="content">
                <section className="form">
                    <h1> User Detail</h1>
                    <Link clasName="back-link" to="/">
                        <FiArrowLeft size={16}/>
                        Back
                    </Link>
                </section>

                <section>
                    <strong>Github Id:</strong>
                    <p>{userDetail.github_id}</p>
                    <strong>Node Id:</strong>
                    <p>{userDetail.node_id}</p>
                    <strong>Username:</strong>
                    <p>{userDetail.username}</p>
                    <strong>Avatar Url:</strong>
                    <p>{userDetail.avatar_url}</p>
                    <strong>Name:</strong>
                    <p>{userDetail.name}</p>
                    <strong>Company:</strong>
                    <p>{userDetail.company}</p>
                    <strong>Blog:</strong>
                    <p>{userDetail.blog}</p>
                    <strong>Location:</strong>
                    <p>{userDetail.location}</p>
                    <strong>Email:</strong>
                    <p>{userDetail.email}</p>
                    <strong>Bio:</strong>
                    <p>{userDetail.bio}</p>
                    <strong>Public Repos:</strong>
                    <p>{userDetail.public_repos}</p>
                    <strong>Followers:</strong>
                    <p>{userDetail.followers}</p>
                    <strong>Following:</strong>
                    <p>{userDetail.following}</p>
                </section>
            </div>
        </div>
    )
}