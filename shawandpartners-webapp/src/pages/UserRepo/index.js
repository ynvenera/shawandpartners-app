import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";

import './style.css';
import api from "../../services/api";

export default function UserRepo(){
    const { username } = useParams();

    const [repos, setRepos] = useState([]);

    useEffect(() => {
        api.get(`api/users/${username}/repos`).then(response => {
            setRepos(response.data)
        })
    })

    return (
        <div className="user-container">
            <h1> Github Users Repositories</h1>
            <ul>
                {repos.map(repo => (
                        <li>
                            <strong>Github Id:</strong>
                            <p>{repo.github_id}</p>
                            <strong>Node Id:</strong>
                            <p>{repo.node_id}</p>
                            <strong>Repository Name:</strong>
                            <p>{repo.name}</p>
                            <strong>Repository Full Name:</strong>
                            <p>{repo.full_name}</p>
                            <strong>description:</strong>
                            <p>{repo.description}</p>
                            <strong>fork:</strong>
                            <p>{repo.fork}</p>
                        </li>
                    )
                )}
            </ul>
        </div>
    )
}