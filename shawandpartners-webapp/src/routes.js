import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";

import User from './pages/User'
import UserDetail from './pages/UserDetail'
import UserRepo from './pages/UserRepo'

export default function AppRoutes(){
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' exact element={<User/>}/>
                <Route path='/details/:username' element={<UserDetail/>}/>
                <Route path='/repos/:username' element={<UserRepo/>}/>
            </Routes>
        </BrowserRouter>
    )
}